# EMF Infrared

I went to [EMF Camp 2024](https://www.emfcamp.org/) and took lots of Infrared photos using a [Topdon TC004 Infrared Camera](https://shkspr.mobi/blog/2023/02/gadget-review-topdon-tc004-infrared-camera/)

* `/JPG` contains false-colour images (in a variety of palettes).
* `/MP4` contains false-colour videos (there is no audio).
* `/Originals` contains the raw IRG files for the images so you can make your own.

You can [read more about extracting data from IRG files](https://shkspr.mobi/blog/2023/02/reverse-engineering-the-irg-infrared-thermal-imaging-format-help-needed/).

All filenames use RFC3339 / ISO8601 datetime stamps. I've tried to give each a distintive name. If you recognise people, please leave a comment or send a pull request.

Enjoy!

## Licence

CC BY-SA
